<?php global $num_tot; ?>
<?php global $fns_css; ?>



<link rel="stylesheet" type="text/css" href="<?php echo $fns_css . 'services.css'; ?>">

<section id="services">
	<div class="container-fluid">
		<div class="row">
			<h2 class="mx-auto"> Services </h2>
		</div>
		<div class="row mx-auto">
			<div class="col-lg-3 text-center">
				<i class="material-icons" data-toggle="modal" data-target="#share" title="Shared Files"> share </i>
			</div>
			<div class="col-lg-3 text-center">
				<i class="material-icons" data-toggle="modal" data-target="#code" title="Coding Service"> code </i>
			</div>
			<div class="col-lg-3 text-center">
				<i class="material-icons" data-toggle="modal" data-target="#cloud" title="SSL e SSH connections"> cloud </i>
			</div>
			<div class="col-lg-3 text-center">
				<i class="material-icons" data-toggle="modal" data-target="#update" totle="Backup Data"> update </i>
			</div>
		</div>
		<div class="row">
			<h2 class="mx-auto"> Our Customers </h2>
		</div>
		<div class="row mx-auto text-center">
			<div class="col-lg-6">
				<i class="material-icons customers"> people </i>
			</div>
			<div class="col-lg-6">
				<p class="customers"> <?php echo $num_tot; ?> </p>
			</div>
		</div>
	</div>
</section>