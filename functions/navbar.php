<?php global $user_access; ?>
<?php global $fns_css; ?>

<link rel="stylesheet" type="text/css" href="<?php echo $fns_css . 'navbar.css'; ?>">


<nav class="navbar bg-dark navbar-dark navbar-expand-lg fixed-top">
  <a class="navbar-brand" href="#">Sharekfile</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
    </ul>
    <ul class="navbar-nav navbar-right">
    	<li class="nav-item">
    		<a href="#about" class="nav-link"> About </a>
    	</li>
    	<li class="nav-item">
    		<a href="#services" class="nav-link"> Services </a>
    	</li>
    	<li class="nav-item">
    		<a href="#prices" class="nav-link"> Prices </a>
    	</li>
    	<li class="nav-item">
    		<a href="#contact" class="nav-link"> Contact </a>
    	</li>
    	<li class="nav-item">
    		<a href="https://os.sharekfile.com" class="nav-link" target="_blank"> OS </a>
    	</li>
        <?php
            if(isset($user_access) && $user_access == true){
                ?>
                    <li class="nav-item">
                        <a href="<?php echo u_area; ?>" class="nav-link"> User Area </a>
                    </li>
                <?php
            }
            else{
                ?>
                <li class="nav-item">
                    <a href="<?php echo login; ?>" class="nav-link"> Login </a>
                </li>
                <li class="nav-item">
                    <a href="<?php echo registration; ?>" class="nav-link"> Registration </a>
                </li>
                <?php
            }
        ?>
    </ul>
  </div>
</nav>