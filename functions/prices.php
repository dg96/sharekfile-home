<?php global $fns_css; ?>

<link rel="stylesheet" type="text/css" href="<?php echo $fns_css . 'prices.css'; ?>">

<section id="prices">
	<div class="container-fluid">
		<div class="row">
			<h2 class="mx-auto"> Prices </h2>
		</div>
		<div class="row text-center">
			<div class="col-lg-4">
				<div class="card mb-4 shadow-sm">
					<div class="card-header">
						<h4 class="my-0 font-weight-normal">Free</h4>
					</div>
					<div class="card-body">
						<h1 class="card-title pricing-card-title">€0 <small class="text-muted">/ mo</small></h1>
						<ul class="list-unstyled mt-3 mb-4">
							<li>All SharekFile's basic Features</li>
							<li><b>20 GB</b> of storage</li>
							<li><b>Limitated</b> Upload Band</li>
							<li><b>FTP</b> No accouny</li>
							<li><b>Coding</b> Not disponible</li>
							<li><b>Free Support</b></li>
						</ul>
						<button type="button" class="btn btn-lg btn-block btn-outline-primary">Sign up for free</button>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="card mb-4 shadow-sm">
					<div class="card-header">
						<h4 class="my-0 font-weight-normal">Basic</h4>
					</div>
					<div class="card-body">
						<h1 class="card-title pricing-card-title">€5 <small class="text-muted">/ mo</small></h1>
						<ul class="list-unstyled mt-3 mb-4">
							<li>All SharekFile's basic Features</li>
							<li><b>100 GB</b> Archiviation</li>
							<li><b>Limitated</b> Upload Band</li>
							<li><b>FTP</b> One Account</li>
							<li><b>Coding</b> Disponible</li>
							<li><b>Basic Support</b></li>
						</ul>
						<a href="https://falco96.com/product/basic-plane-cloud" class="btn btn-lg btn-block btn-outline-primary" target="_blamk"> Order Now </a>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="card mb-4 shadow-sm">
					<div class="card-header">
						<h4 class="my-0 font-weight-normal">Basic Plus</h4>
					</div>
					<div class="card-body">
						<h1 class="card-title pricing-card-title">€15 <small class="text-muted">/ mo</small></h1>
						<ul class="list-unstyled mt-3 mb-4">
							<li>All SharekFile's basic Features</li>
							<li><b>200 GB </b> Archiviation</li>
							<li><b>Limitated</b> Upload Band</li>
							<li><b>FTP</b> One account</li>
							<li><b>Coding</b> Disponible</li>
							<li><b>Basic Support</b></li>
						</ul>
						<a href="https://falco96.com/product/basic-plus-cloud" class="btn btn-lg btn-block btn-outline-primary" target="_blamk"> Order Now </a>
					</div>
				</div>
			</div>
		</div>
		<div class="row text-center">
			<div class="col-lg-6">
				<div class="card mb-4 shadow-sm">
					<div class="card-header">
						<h4 class="my-0 font-weight-normal">Basic 300GB</h4>
					</div>
					<div class="card-body">
						<h1 class="card-title pricing-card-title">€25 <small class="text-muted">/ mo</small></h1>
						<ul class="list-unstyled mt-3 mb-4">
							<li>All SharekFile's basic Features</li>
							<li><b>300 GB</b> Archiviation</li>
							<li><b>Unlocked</b> Upload Band</li>
							<li><b>FTP</b> One accouny</li>
							<li><b>Coding</b> disponible</li>
							<li><b>Premium Support</b></li>
						</ul>
						<a href="https://falco96.com/product/basic-300gb-cloud" class="btn btn-lg btn-block btn-outline-primary" target="_blamk"> Order Now </a>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="card mb-4 shadow-sm">
					<div class="card-header">
						<h4 class="my-0 font-weight-normal">Pro</h4>
					</div>
					<div class="card-body">
						<h1 class="card-title pricing-card-title">€70 <small class="text-muted">/ mo</small></h1>
						<ul class="list-unstyled mt-3 mb-4">
							<li>All SharekFile's basic Features</li>
							<li><b>2 TB</b> Archivation</li>
							<li><b>Unlocked</b> Upload Band</li>
							<li><b>FTP</b> Unlimited accounts</li>
							<li><b>Domain</b> One of request</li>
							<li><b>Database:</b> Unlimited dedicated</li>
							<li><b>Unlimited</b> email address</li>
							<li><b>Coding</b> Not disponible</li>
							<li><b>Premium Support</b></li>
						</ul>
						<a href="https://falco96.com/product/sharekfile-pro-plane" class="btn btn-lg btn-block btn-outline-primary" target="_blamk"> Order Now </a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>