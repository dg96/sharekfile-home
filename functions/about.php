<?php global $fns_css; ?>
<?php global $fns_img; ?>

<link rel="stylesheet" type="text/css" href="<?php echo $fns_css . 'about.css'; ?>">

<section id="about">
	<div class="container-fluid">
		<div class="row">
			<h2 class="mx-auto"> About Me </h2>
		</div>
		<div class="row">
			<div class="col-lg-12 mx-auto">
				<div class="card mx-auto" style="width: 18rem;">
					<img class="card-img-top" src="<?php echo $fns_img . 't_developer.jpg'; ?>" alt="Card image cap">
					<div class="card-body">
						<h5 class="card-title"><a href="https://teck-developer.com" target="_blank"> Tech Developer </a></h5>
						<p class="card-text">
							Tech Developer is a new agency made from Denis Genitoni. This society manage and have been created this cloud, for help
							siple customers, developers and other agency to save and elaborate the data everywere, without a specific device.
						</p>
						<a href="https://teck-developer.com" target="_blank" class="btn btn-outline-primary">Vai al sito principale</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>