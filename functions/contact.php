<?php global $fns_css; ?>

<link rel="stylesheet" type="text/css" href="<?php echo $fns_css . 'contact.css'; ?>">

<section id="contact">
	<div class="container-fluid">
		<div class="row">
			<h2 class="mx-auto"> Contact Me </h2>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<form method="post" action="">
					<div class="inline-form">
						<input type="text" name="name" id="name" class="form-control" placeholder="Name" required>
						<input type="text" name="surname" id="surname" class="form-control" placeholder="Surname" required>
					</div>
					<div class="form-group">
						<input type="email" name="email" id="email" class="form-control" placeholder="Email" required>
					</div>
					<div class="form-group">
						<input type="text" name="object" id="object" class="form-control" placeholder="Object" required>
					</div>
					<div class="form-group">
						<textarea name="text" id="text" class="form-control"></textarea>
					</div>
					<div class="form-group">
						<input type="submit" name="sendData" id="sendData" class="btn btn-outline-primary" value="Contact Me">
					</div>
				</form>
			</div> 
			<div class="col-lg-6">
				<ul>
					<li> <i class="material-icons"> email </i> <a href="mailto: denis.genitoni@teck-developer.com"> denis.genitoni@teck-developer.com </a> </li>
					<li><i class="material-icons"> phone </i> <a href="tel: 3203071917"> 3203071917 </a> </li>
				</ul>
				<div class="form-group">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d22711.254216655183!2d10.36231798903744!3d44.63982226236984!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47800dd264abd5c3%3A0x41beed1ed97ddf64!2s43029+Traversetolo%2C+Province+of+Parma!5e0!3m2!1sen!2sit!4v1541862457984" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>
</section>