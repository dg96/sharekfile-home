<?php global $fns_css; ?>
<link rel="stylesheet" type="text/css" href="<?php echo $fns_css . 'footer.css'; ?>">

	<!-- Footer -->
			<footer class="page-footer font-small blue pt-4">

				<!-- Footer Links -->
				<div class="container-fluid text-center text-md-left">

					<!-- Grid row -->
					<div class="row">

						<!-- Grid column -->
						<div class="col-md-6 mt-md-0 mt-3">

							<!-- Content -->
							<h5 class="text-uppercase">Tech Developer</h5>
							<p>
								P.IVA: 02871690349 <br />
								Azienda registrata a maggio 2018 a nome di Denis Genitoni <br />
								A destra potrai trovare tutti i miei servizi, sviluppati nel tempo <br />
								Sono disponibile ai contatti sopra riportati dal lunedì al venerdì 7:30 / 19:00 <br />
								Per richieste commerciali e non lavorative: <a href="mailto: info@teck-developer.com"> info@teck-developer.com </a>
							</p>

						</div>
						<!-- Grid column -->

						<hr class="clearfix w-100 d-md-none pb-3">

						<!-- Grid column -->
						<div class="col-md-3 mb-md-0 mb-3">

							<!-- Links -->
							<h5 class="text-uppercase">Servizi</h5>

							<ul class="list-unstyled">
								<li>
									<a href="https://sharekfile.com" target="_blank">SharekFile Cloud</a>
								</li>
								<li>
									<a href="https://falco96.com" target="_blank">Falco96 blog</a>
								</li>
								<li>
									<a href="https://falco96.com/shop/" target="_blank">Falco96 acquisti</a>
								</li>
							</ul>

						</div>
						<!-- Grid column -->

						<!-- Grid column -->
						<div class="col-md-3 mb-md-0 mb-3">

							<!-- Links -->
							<h5 class="text-uppercase">Prodotti</h5>

							<ul class="list-unstyled">
								<li>
									<a href="https://os.sharekfile.com" target="_blank">SharekFileOS</a>
								</li>
							</ul>

						</div>
						<!-- Grid column -->

					</div>
					<!-- Grid row -->

				</div>
				<!-- Footer Links -->

				<!-- Copyright -->
				<div class="footer-copyright text-center py-3">© 2018 Copyright:
					<a href="https://teck-developer.com" target="_blank"> Tech Developer</a>
				</div>
				<!-- Copyright -->

			</footer>
			<!-- Footer -->