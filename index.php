<?php include 'config.php'; ?>
<?php include(info_account); ?>

<?php
	if(isset($_POST['sendData'])){
		formValidate();
	}
?>

<?php
$session_p = root_site_p . "functions/configurations/sessions/session.php";
include($session_p);

$values = session_log_new();

$username = $values[1];

if($username != ""){
	$user_access = true;
}
?>

<?php

// users number

    $sql = "SELECT COUNT(*) FROM users";
    $sel_numUsers = sendQuery($sql, error_seldb, 'select');
    
    $row = mysqli_fetch_row($sel_numUsers);
    $num = $row[0];

    $sql = "SELECT COUNT(*) FROM hosting_plane";
    $sel_numHosts = sendQuery($sql, error_seldb, 'select');
    
    $row = mysqli_fetch_row($sel_numHosts);
    $num_hosts = $row[0];

    $num_tot = $num + $num_hosts;

    // fns directory

    $fns = root_site_p . "functions/bootstrap-4.0.0-dist/homePage/fns/";
    $fns_css = url . "/functions/bootstrap-4.0.0-dist/homePage/css/";
    $fns_js = url . "/functions/bootstrap-4.0.0-dist/homePage/js/";
    $fns_img = url . "/functions/bootstrap-4.0.0-dist/homePage/img/";

    // favicon
    $favicon = url . "/functions/graphic/img/favicon/favicon-falco96.png";

?>

<?php include $fns . 'functions.php'; ?>
<?php include $fns . 'formValidate.php'; ?>

<!DOCTYPE html>
<html>
<head>
	<?php head(); ?>
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="50">
	<!-- Navbar -->
	<?php include  $fns . 'navbar.php'; ?>
	<!-- Navbar -->

	<!-- Header -->
	<?php include $fns . 'header.php'; ?>
	<!-- Header -->

	<!-- About -->
	<?php include $fns . 'about.php'; ?>
	<!-- About -->

	<!-- Services -->
	<?php include $fns . 'services.php'; ?>
	<!-- Services -->

	<!-- Prices -->
	<?php include $fns . 'prices.php'; ?>
	<!-- Prices -->

	<!-- Contact -->
	<?php include $fns . 'contact.php'; ?>
	<!-- Contact -->


	<!-- Footer -->
	<?php include $fns . 'footer.php'; ?>
	<!-- Footer -->

	
	<!-- Modals -->
	<?php modalsServices(); ?>
	<!-- Modals -->

	<?php js(); ?>
</body>
</html>